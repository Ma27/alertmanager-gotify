# gotify-alertmanager
 
[![pipeline status](https://img.shields.io/endpoint?url=https://hydra.ist.nicht-so.sexy/job/alertmanager-gotify/master/tests.integration-test.x86_64-linux/shield)](https://hydra.ist.nicht-so.sexy/job/alertmanager-gotify/master/tests.integration-test.x86_64-linux)

Simple bridge which sends [alertmanager](https://prometheus.io/docs/alerting/alertmanager/)
notifications to [gotify](https://gotify.net). Continuation of
[`git.sbruder.de/simon/alertmanager-gotify`](https://git.sbruder.de/simon/alertmanager-gotify).

## Build

``` bash
$ nix build .#checks.x86_64-linux.integration-test.driver -L
```

## Update

```
$ nix develop
$ go get -u ./...
$ nix flake update --impure --recreate-lock-file --commit-lock-file
```
