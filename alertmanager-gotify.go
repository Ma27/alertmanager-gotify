package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"sort"
	"strings"

	"errors"
	api "github.com/gotify/go-api-client/v2/client"

	"github.com/gotify/go-api-client/v2/auth"
	"github.com/gotify/go-api-client/v2/client/message"
	"github.com/gotify/go-api-client/v2/gotify"
	"github.com/gotify/go-api-client/v2/models"
)

type POSTData struct {
	Alerts            []Alert           `json:"alerts"`
	Status            string            `json:"status"`
	CommonLabels      map[string]string `json:"commonLabels"`
	GroupLabels       map[string]string `json:"groupLabels"`
	CommonAnnotations map[string]string `json:"commonAnnotations"`
	ExternalURL       string            `json:"externalURL"`
}

type Alert struct {
	Annotations  map[string]string `json:"annotations"`
	Labels       map[string]string `json:"labels"`
	Status       string            `json:"status"`
	GeneratorURL string            `json:"generatorURL"`
}

func main() {
	gotifyURL := os.Getenv("GOTIFY_URL")

	http.HandleFunc("/alert", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			http.Error(w, "405 Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		applicationToken := r.URL.Query().Get("token")
		if applicationToken == "" {
			http.Error(w, "401 Unauthorized", http.StatusUnauthorized)
			return
		}

		body, err := io.ReadAll(r.Body)
		if err != nil {
			log.Printf("Error reading request body %v", err)
			http.Error(w, "400 Internal Server Error", http.StatusBadRequest)
			return
		}
		postData := POSTData{}
		err = json.Unmarshal(body, &postData)
		if err != nil {
			log.Printf("Failed to parse json %v", err)
			http.Error(w, "400 Internal Server Error", http.StatusBadRequest)
			return
		}

		parsedURL, _ := url.Parse(gotifyURL)
		client := gotify.NewClient(parsedURL, &http.Client{})

		// print all group label values in title
		groupLabels := make([]string, 0, len(postData.GroupLabels))
		for _, val := range postData.GroupLabels {
			groupLabels = append(groupLabels, val)
		}
		sort.Strings(groupLabels)

		// don't reprint those
		for k := range postData.CommonLabels {
			_, ok := postData.GroupLabels[k]
			if ok {
				delete(postData.CommonLabels, k)
			}
		}

		commonLabelsList := make([]string, 0, len(postData.CommonLabels))
		if len(postData.CommonLabels) > 0 {
			for _, val := range postData.CommonLabels {
				commonLabelsList = append(commonLabelsList, val)
			}
			// Hack to ease integration testing
			sort.Strings(commonLabelsList)
		}

		resolved := []Alert{}
		firing := []Alert{}

		for _, entry := range postData.Alerts {
			if entry.Status == "resolved" {
				resolved = append(resolved, entry)
			} else {
				firing = append(firing, entry)
			}
		}

		send_firing := sendNotification(firing, "firing", client, groupLabels, commonLabelsList, postData, applicationToken, w)
		send_resolved := sendNotification(resolved, "resolved", client, groupLabels, commonLabelsList, postData, applicationToken, w)

		if !send_firing || !send_resolved {
			http.Error(w, "Failed sending notifications", http.StatusInternalServerError)
		}
	})

	err := http.ListenAndServe(
		fmt.Sprintf(
			"%s:%s",
			envvar("GOTIFY_ALERTMANAGER_LISTEN_ADDR", ""),
			envvar("GOTIFY_ALERTMANAGER_PORT", "8081"),
		),
		nil,
	)

	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error starting server: %s\n", err)
		os.Exit(1)
	}
}

func sendNotification(alerts []Alert, status string, client *api.GotifyREST, groupLabels []string, commonLabels []string, rawPostData POSTData, applicationToken string, w http.ResponseWriter) bool {
	n_alerts := len(alerts)
	if n_alerts == 0 {
		return true
	}

	var message_title strings.Builder
	var message_body strings.Builder

	message_title.WriteString("[")
	if status == "resolved" {
		message_title.WriteString("RESOLVED:")
	} else {
		message_title.WriteString("FIRING:")
	}

	message_title.WriteString(fmt.Sprintf("%d] ", n_alerts))
	message_title.WriteString(strings.Join(groupLabels, " "))

	// show all remaining common labels
	if len(commonLabels) > 0 {
		commonLabelsList := append(make([]string, 0, len(commonLabels)), commonLabels...)
		// Hack to ease integration testing
		sort.Strings(commonLabelsList)
		message_title.WriteString(fmt.Sprintf(" (%s)", strings.Join(commonLabelsList, " ")))
	}

	// create the body (common annotations, then all alerts)
	message_body.WriteString(fmt.Sprintf(
		"View in AlertManager: [%s](%s)\n\n",
		rawPostData.ExternalURL,
		rawPostData.ExternalURL,
	))
	commonAnnotations := make([]string, 0, len(rawPostData.CommonAnnotations))
	for _, val := range rawPostData.CommonAnnotations {
		commonAnnotations = append(commonAnnotations, val)
	}
	sort.Strings(commonAnnotations)
	message_body.WriteString(strings.Join(commonAnnotations, " "))

	for _, alert := range alerts {
		message_body.WriteString("\n\nLabels:\n")
		label_keys := make([]string, 0, len(alert.Labels))
		for k := range alert.Labels {
			label_keys = append(label_keys, k)
		}
		sort.Strings(label_keys)
		for _, k := range label_keys {
			message_body.WriteString(fmt.Sprintf("- %s = %s\n", k, alert.Labels[k]))
		}
		message_body.WriteString("Annotations:\n")
		for k, v := range alert.Annotations {
			message_body.WriteString(fmt.Sprintf("- %s = `%s`\n", k, v))
		}
		message_body.WriteString(fmt.Sprintf(
			"Source: [%s](%s)\n",
			alert.GeneratorURL,
			alert.GeneratorURL,
		))
	}

	var prio int
	if status == "resolved" {
		prio = 4
	} else {
		prio = 6
	}

	params := message.NewCreateMessageParams()
	params.Body = &models.MessageExternal{
		Title:    message_title.String(),
		Message:  message_body.String(),
		Extras:   map[string]interface{}{"client::display": map[string]string{"contentType": "text/markdown"}},
		Priority: prio,
	}
	_, err := client.Message.CreateMessage(params, auth.TokenAuth(applicationToken))

	if err != nil {
		log.Printf("Could not send message %v", err)
		return false
	}
	return true
}

func envvar(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	} else {
		return value
	}
}
