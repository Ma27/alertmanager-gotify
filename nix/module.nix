{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.alertmanager-gotify;
in {
  options = {
    services.alertmanager-gotify = {
      enable = mkEnableOption "alertmanager-gotify";

      gotifyURL = mkOption {
        type = types.str;
        example = "https://pager.ausguck.mbosch.me/";
        description = ''
          Public URL of the Gotify server.
        '';
      };

      package = mkOption {
        type = types.package;
        default = pkgs.alertmanager-gotify;
        defaultText = "pkgs.alertmanager-gotify";
        description = ''
          Package to use.
        '';
      };

      port = mkOption {
        type = types.port;
        default = 8081;
        description = ''
          Listen port of the alert-manager bridge.
        '';
      };

      address = mkOption {
        type = types.nullOr types.str;
        default = null;
        description = ''
          Listen address of the alert-manager bridge.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.alertmanager-gotify-bridge = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];

      environment = {
        GOTIFY_URL = cfg.gotifyURL;
        GOTIFY_ALERTMANAGER_LISTEN_ADDR = optionalString (cfg.address != null) cfg.address;
        GOTIFY_ALERTMANAGER_PORT = toString cfg.port;
      };

      serviceConfig = {
        DynamicUser = "yes";
        ExecStart = "${cfg.package}/bin/alertmanager-gotify";
        LockPersonality = "yes";
        PrivateDevices = "yes";
        PrivateMounts = "yes";
        PrivateUsers = "yes";
        ProtectClock = "yes";
        ProtectControlGroups = "yes";
        ProtectHome = "yes";
        ProtectKernelTunables = "yes";
        ProtectSystem = "full";
        Restart = "always";
        RestrictAddressFamilies = "AF_UNIX AF_INET AF_INET6";
        RestrictNamespaces = "yes";
        RestrictRealtime = "yes";
        RestrictSUIDSGID = "yes";
        SystemCallArchitectures = "native";
      };
    };
  };
}
