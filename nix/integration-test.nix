{ pkgs, module }:

with import (pkgs.path + "/nixos/lib/testing-python.nix") {
  inherit (pkgs.stdenv) system;
};

let
  gotifyToken = "foobar";

  inherit (pkgs) writeText;
in

makeTest {
  name = "gotify-alertmanager";
  meta = with pkgs.lib.maintainers; {
    maintainers = [ ma27 ];
  };

  nodes = {
    alertmanager = { pkgs, lib, config, ... }: {
      environment.systemPackages = [ pkgs.jq ];
      services.prometheus = {
        enable = true;
        alertmanagers = [{
          scheme = "http";
          path_prefix = "/";
          static_configs = [{
            targets = [ "localhost:9093" ];
          }];
        }];
        extraFlags = ["--log.level=debug"];
        globalConfig.evaluation_interval = "1s";
        scrapeConfigs = [
          { job_name = "general";
            scrape_interval = "1s";
            static_configs = [{
              targets = [ "localhost:9091" ];
            }];
          }
        ];
        rules = [ (builtins.toJSON {
          groups = [{
            name = "general";
            rules = [
              { alert = "pushgw_val";
                expr = "pushgw_val{job='general'}==1";
                for = "1s";
                labels.severity = "page";
                annotations.summary = "Demo alert!";
                annotations.description = "body";
                annotations.title = "Demo alert!";
              }
              { alert = "pushgw_summary";
                expr = "pushgw_summary{job='general'}==1";
                for = "1s";
                labels.severity = "page";
                annotations.summary = "Demo alert summary!";
                annotations.description = "body";
              }
              { alert = "pushgw_no_summary";
                expr = "pushgw_no_summary{job='general'}==1";
                for = "1s";
                labels.severity = "page";
                annotations.description = "nosummary";
              }
            ];
          }];
        }) ];
      };
      services.prometheus.pushgateway = {
        enable = true;
        web.listen-address = ":9091";
        persistMetrics = true;
        persistence.interval = "1s";
        stateDir = "prometheus-pushgateway";
      };
      services.prometheus.alertmanager = {
        enable = true;
        port = 9093;
        logLevel = "debug";
        configuration = {
          route = {
            receiver = "default";
            group_interval = "1s";
            group_wait = "1s";
            group_by = [ "instance" ];
          };
          receivers = [{
            name = "default";
            webhook_configs = [
              { url = "http://gotify:8082/alert?token=${gotifyToken}";
                send_resolved = true;
              }
            ];
          }];
        };
      };
    };
    gotify = { pkgs, lib, config, ... }: {
      environment.systemPackages = with pkgs; [ jq sqlite ];
      imports = [ module ];
      networking.firewall.allowedTCPPorts = [ 8082 ];
      services.gotify = {
        enable = true;
        port = 3000;
      };
      services.alertmanager-gotify = {
        enable = true;
        gotifyURL = "http://localhost:3000/";
        port = 8082;
      };
    };
  };

  testScript = let
    metric = writeText "metric" ''
      # HELP simple true/false flag
      # TYPE pushgw_val gauge
      pushgw_val 1
    '';
    metric2 = writeText "metric2" ''
      # HELP simple true/false flag
      # TYPE pushgw_val gauge
      pushgw_val 0
    '';
    metric3 = writeText "metric3" ''
      # HELP simple true/false flag 1
      # TYPE pushgw_summary gauge
      pushgw_summary 1
    '';
    metric4 = writeText "metric4" ''
      # HELP simple true/false flag 2
      # TYPE pushgw_no_summary gauge
      pushgw_no_summary 1
    '';
  in ''
    def retrieve_gotify_token():
        return gotify.succeed(
            "curl --fail -sS -X POST localhost:3000/client -F name=nixos "
            + '-H "Authorization: Basic $(echo -ne "admin:admin" | base64 --wrap 0)" '
            + "| jq .token | xargs echo -n"
        )


    def send_metrics(metric):
        alertmanager.succeed(
            f"curl -sf --data-binary @- http://localhost:9091/metrics/job/general < {metric}"
        )


    def assert_nth_message_title(usertoken, n, expected):
        actual = gotify.wait_until_succeeds(
            "set -o pipefail; "
            + f"curl --fail -sS 'localhost:3000/message?since=0&token={usertoken}' | "
            + f"jq -e '.messages[{n}].title' | xargs echo -n"
        )

        assert actual == expected, f"Wrong message! Expected {expected}, got {actual} (at {n})."

    def assert_nth_description(usertoken, n, expected):
        actual_out = gotify.wait_until_succeeds(
            "set -o pipefail; "
            + f"curl --fail -sS 'localhost:3000/message?since=0&token={usertoken}' | "
            + f"jq -re '.messages[{n}].message'"
        ).split("\n")
        print(actual_out)
        actual = actual_out[2]

        assert actual == expected, f"Wrong message! Expected {expected}, got {actual} (at {n})."


    def check_latest_alert(msg, field = "summary"):
        alertmanager.wait_until_succeeds(
            'curl -fSs "http://localhost:9093/api/v2/alerts/groups?silenced=false&active=true" | '
            + f"jq '.[0].alerts[0].annotations.{field}' | grep '{msg}'"
        )


    start_all()

    gotify.wait_for_open_port(3000)  # gotify-server
    gotify.wait_for_open_port(8082)  # alertmanager-bridge
    alertmanager.wait_for_open_port(9090)  # prometheus
    alertmanager.wait_for_open_port(9091)  # pushgateway
    alertmanager.wait_for_open_port(9093)  # alertmanager

    gotify.succeed("curl -fsS localhost:3000/")
    gotify.succeed(
      """
      sqlite3 /var/lib/gotify-server/data/gotify.db \
        "INSERT INTO applications (id,token,user_id,name,description,internal) VALUES (1,'${gotifyToken}',1,'NixOS Test','...',0)"
      """
    )

    # pushgw_val=1 -> 'Demo alert!'
    send_metrics("${metric}")
    check_latest_alert("Demo alert!")

    usertoken = retrieve_gotify_token()
    assert_nth_message_title(usertoken, 0, "[FIRING:1] localhost:9091 (general general page pushgw_val)")

    # pushgw_val=0 -> resolve
    send_metrics("${metric2}")
    alertmanager.wait_until_succeeds(
        'test "[]" = "$(curl -fSs "http://localhost:9093/api/v2/alerts/groups?silenced=false&active=true")"'
        # + "jq -r ')\""
    )

    # pushgw_summary=1 -> 'Demo alert summary!'
    send_metrics("${metric3}")
    check_latest_alert("Demo alert summary!")

    # pushgw_no_summary=1 -> Alert w/o summary.
    send_metrics("${metric4}")
    check_latest_alert("nosummary", field="description")

    alertmanager.sleep(10)

    print(gotify.succeed(f"curl http://localhost:3000/message?token={usertoken} | jq"))

    assert_nth_message_title(usertoken, 2, "[RESOLVED:1] localhost:9091 (general general page pushgw_val)")
    assert_nth_description(usertoken, 2, "Demo alert! Demo alert! body")
    assert_nth_message_title(usertoken, 1, "[FIRING:1] localhost:9091 (general general page pushgw_summary)")
    assert_nth_description(usertoken, 1, "Demo alert summary! body")
    assert_nth_message_title(usertoken, 3, "[FIRING:1] localhost:9091 (general general page pushgw_val)")
    assert_nth_description(usertoken, 3, "Demo alert! Demo alert! body")

    gotify.shutdown()
    alertmanager.shutdown()
  '';
}
