{
  description = "Get notified about prometheus alerts with gotify";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-24.11";
  };

  outputs = { self, nixpkgs }: let
    inherit (nixpkgs) lib;
    inherit (lib) fileset;
    baseFiles = fileset.fileFilter (f: f.name != ".gitignore") ./.;

    eachSystem = lib.genAttrs [ "x86_64-linux" ];

    pkgs = eachSystem (system: nixpkgs.legacyPackages.${system}.extend self.overlays.default);
  in {
    overlays.default = final: prev: {
      alertmanager-gotify = final.buildGoModule {
        pname = "alertmanager-gotify";
        version = "0.1.2-dev";
        src = fileset.toSource {
          root = ./.;
          fileset = fileset.intersection baseFiles (
            fileset.unions [
              ./go.mod
              ./go.sum
              ./alertmanager-gotify.go
            ]
          );
        };

        nativeCheckInputs = [ final.golangci-lint ];
        doCheck = true;
        checkPhase = ''
          runHook preCheck
          export HOME="$(mktemp -d)"
          golangci-lint run
          fmt_diff="$(gofmt -d *.go)"
          if [ -n "$fmt_diff" ]; then
            echo -e "\e[31mFormatter failed!\e[0m"
            echo "$fmt_diff"
            exit 1
          fi
          runHook postCheck
        '';

        vendorHash = "sha256-AiwTXlgpRA6rO5uTUdrEGwOmkfYdU9EhbPyZtZfHHqQ=";

        meta = with final.lib; {
          maintainers = with maintainers; [ ma27 ];
          license = licenses.mit;
        };
      };
    };

    nixosModules.alertmanager-gotify = { pkgs, ... }: {
      imports = [ ./nix/module.nix ];
      services.alertmanager-gotify.package =
        self.packages.${pkgs.stdenv.hostPlatform.system}.default;
    };

    packages = eachSystem (system: {
      default = pkgs.${system}.alertmanager-gotify;
      inherit (pkgs.${system}) alertmanager-gotify;
    });

    devShells = eachSystem (system: {
      default = with pkgs.${system}; mkShell {
        packages = [
          go
          gopls
          golangci-lint
        ];
        shellHook = ''
          export GOPATH="$(git rev-parse --show-toplevel)/.go"
          export GOPROXY=off
        '';
      };
    });

    hydraJobs = {
      alertmanager-gotify.x86_64-linux = self.packages.x86_64-linux.alertmanager-gotify;
      tests.integration-test.x86_64-linux = import ./nix/integration-test.nix {
        pkgs = pkgs.x86_64-linux;
        module = self.nixosModules.alertmanager-gotify;
      };
    };
  };
}
